// leetcode 172
// Factorial Trailing Zeroes
package main

import (
	"fmt"
)

func trailingZeroes(n int) int {
	// T = 2^m * 5^r; n! = T * K; result = min(m,r). Because 'm' is always larger than 'r', result = r.
	// The problem is transformed to calculate number of factor '5' of n!.
	// f(n) = f(n/5) + n / 5
	// e.g, (25! -> [25,20,15,10,5] -> [5* 5, 5* 4, 5* 3, 5* 2, 5* 1] -> 5*5 -> [5] -> [5 *1])
	//                                  1     1     1     1     1                       1
	//                                              5                  +                1
	// f(25) =  f(5) + 5 =                                             6
	count := 0
	for n != 0 {
		count += n / 5
		n /= 5
	}
	return count
}

func main() {
	fmt.Println(trailingZeroes(100))
}
