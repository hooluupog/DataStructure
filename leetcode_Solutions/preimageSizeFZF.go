// leetcode 793
// Preimage Size of Factorial Zeroes Function
package main

import (
	"fmt"
)

func preimageSizeFZF(k int) int {
	// k < 5, k = k, x = [5k, 5k+1, ..., 5k+4], result = 5.
	// k = 5, result = 0, the integer 'x' does not exist.
	// k > 5,
	// k = A0 + A1 + A2 + ... + An = A1/5 + A2/5 + A3/5 + ... + An (The equation is unique.)
	// An = [5m, 5m+1, ... , 5m+4].
	// The problem is transformed to find An.
	if k < 5 {
		return 5
	}
	if k == 5 {
		return 0
	}
	count := 0
	for i := k / 5; i >= 1; i-- { // find An which is 5*m.
		sum := i * 5
		for tmp := sum / 5; tmp >= 1 && sum <= k; {
			sum += tmp
			tmp /= 5
		}
		if sum == k {
			count += 5
			break
		} else if sum < k { // Try with An = [5m+1, 5m+2, 5m+3,5m+4]
			if sum+1 == k || sum+2 == k || sum+3 == k || sum+4 == k {
				count += 5
				break
			}
		}
	}
	return count
}

func main() {
	fmt.Println(preimageSizeFZF(1000000000))
}
