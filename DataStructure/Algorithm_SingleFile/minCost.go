package main

import (
	"fmt"
	"sort"
)

// poj 3666
// 修一条海拔单调增或单调减的路，修之前的海拔是A[i]，修之后是B[i]，花费|A[i] - B[i]|。求最小花费。
// 分析：最小花费下，B中的数都可以在A中出现过（如果两个数调整后的数值在两个数中间，
// 那么可以把两个数都变为其中一个数，而代价显然是等同的)。

// b[j] = sorted(a[j]) ,(b[j] >= b[j-1])
// dp[i][j] = min{dp[i-1][k]} + abs(a[i] - b[j])  k = 0,1,2,...,j
// dp[0][j] = abs(a[0] - b[j]) j = 0,1,2,...,n-1

func minValue(a []int) int {
	min := a[0]
	for _, v := range a {
		if min > v {
			min = v
		}
	}
	return min
}

func abs(a int) int {
	if a > 0 {
		return a
	}
	return -a
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func minCost(a []int) int {
	b := make([]int, len(a))
	copy(b, a)
	sort.Slice(b, func(i, j int) bool { return b[i] < b[j] })
	dp := make([][]int, 2)
	for i := range dp {
		dp[i] = make([]int, len(a))
	}
	for i := 0; i < len(a); i++ {
		dp[0][i] = abs(a[0] - b[i])
	}
	// 使用滚动数组优化存储空间
	k := 0
	for i := 1; i < len(a); i++ {
		k = i % 2
		pre := k ^ 1
		tmp := dp[pre][0]
		for j := 0; j < len(a); j++ {
			tmp = min(tmp, dp[pre][j])
			dp[k][j] = tmp + abs(a[i]-b[j])
		}
	}
	return minValue(dp[k])
}

func main() {
	a := []int{1, 3, 2, 4, 5, 3, 9}
	fmt.Println(minCost(a))
}
